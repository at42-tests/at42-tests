DOCKER = docker
VIRTUALBOX = virtualbox
BREW = brew
TAR = tar
CURL = curl
SUDO = sudo
CP = cp
SHELL = /bin/bash
MKDIR = mkdir
RM = rm -f
RAND_STR = $(shell head /dev/urandom | tr -dc A-Za-z0-9 | head -c 16)
MACHINE_NAME := $(RAND_STR)

DOCKER_INSTALLED := $(shell $(DOCKER) --help 2>/dev/null)
VIRTUALBOX_INSTALLED := $(shell $(VIRTUALBOX) --help 2>/dev/null)
BREW_INSTALLED := $(shell $(BREW) --version 2>/dev/null)

NOT_SUPPORTED_STR = You are running an OS which our installer does not support \
					Please install Docker
ifeq ($(OS), Windows_NT)
	DETECTED_OS := Windows
else
	DETECTED_OS := $(shell uname)
endif

install:
ifndef DOCKER_INSTALLED
ifeq ($(DETECTED_OS), Linux)
	$(CURL) https://download.docker.com/linux/static/stable/x86_64/docker-19.03.9.tgz > /tmp/docker.tar.gz
	$(TAR) -xvzf /tmp/docker.tar.gz
	$(SUDO) $(CP) /tmp/docker/* /usr/bin
	$(SUDO) dockerd &
	@echo Installed docker.
else
ifeq ($(DETECTED_OS), Darwin)
ifndef BREW_INSTALLED
	git clone --depth=1 --branch=master https://github.com/homebrew/brew ~/.brew
	echo "export PATH=~/.brew/bin/:$$PATH" >> ~/.zshrc
	@printf "\033[32mBrew installed. To Apply changes, please run \'source ~/.zshrc\'.\n\033[39m"
	@exit 1
endif
ifndef VIRTUALBOX_INSTALLED
	@print "\033[31mThis program requires virtualbox, we couldn't find.\n\033[39m"
	@print "\033[31mInstalling it requires root priviledges.\n\033[39m"
	@print "\033[31mIf you are not root, we'll give you 10 seconds so you can stop this script.\n\033[39m"
	@print "\033[31mIf you are using guacamole, you can logout, and try another computer, \033[39m"
	@print "\033[31mwhich might have virtualbox installed\n\033[39m"
	@sleep 10
	HOMEBREW_NO_AUTO_UPDATE=1 brew cask install virtualbox
endif
	HOMEBREW_NO_AUTO_UPDATE=1 brew install docker docker-machine
	docker-machine create --driver virtualbox $(MACHINE_NAME)
	@printf "\033[32mDocker installed. To apply changes, please run \"echo 'eval $$"
	@printf "(docker-machine env $(MACHINE_NAME))' >> ~/.zshrc && source ~/.zshrc\".\n\033[39m"
	@exit 1
else
	@echo $(NOT_SUPPORTED_STR)
endif
endif
endif

libft: install
	$(DOCKER) pull neirpyc/libft-test:latest
	$(RM) -r ~/.at42-norminette
	git clone https://github.com/42Paris/norminette ~/.at42-norminette
	$(DOCKER) run --cap-add=SYS_PTRACE --rm -it -v ~/.ssh:/home/at42-libft-test/.ssh \
	-v ~/.at42-norminette:/home/at42-libft-test/.norminette neirpyc/libft-test:latest
	$(RM) -r ~/.at42-norminette

.PHONY: install libft check