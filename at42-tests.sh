#!/bin/bash
PS3='Please enter your choice: '
options=("install" "libft" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "install")
            make install
            break
            ;;
        "libft")
            make libft
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
